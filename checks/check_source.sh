#!/bin/bash

if [ `command -v pylint` ]; then
    for file in source/*.py
    do
        pylint $file
    done
fi

if [ ! -f README.md ]; then
    echo "README doesnt exist"
    exit 1
fi
