## Synopsis

This is a Python program which reads a GOES Particle text product and converts it to a JSON.

## Code Example

Run `python3.5 json_builder.py`

## Motivation

We had to for the Jenkins class in which Kiley and Stone were enrolled.

## Installation

Use the Jenkins pipeline which is built as part of this project.

## Tests

Run automatically via pipeline

## Contributors

* Kiley Gray
* David Stone

## License

none
