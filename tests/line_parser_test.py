#! /usr/bin/env python3.5
"""This tests our line parser file."""
#! /usr/bin/env python3.5

import re 

try:
    import unittest2 as unittest
except ImportError:
    import unittest

import ast

import sys
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(dir_path + '/../source')

from line_parser import get_key_value_map

filename = "test_key_value_list.txt"

class SimpleTest(unittest.TestCase):
    """Start of unit test."""
    def test_get_key_value_map(self):
        """This runs the test."""
        expected_dictionary = {}
        key = ""
        value = ""
        line_list = []

        cwd = os.getcwd()
        test_dir = cwd + "/tests/"

        with open(test_dir + filename) as f:
            for line in f:
                line_list.append(line)
        for line in line_list:
            if line == "comment\n":
                if value != "":
                    expected_dictionary[key] = value
                key = line.split("\n")[0]
                value = ""
            elif line == "header\n":
                if value != "":
                    expected_dictionary[key] = value
                key = line.split("\n")[0]
                value = ""
            elif line == "data\n":
                if value != "":
                    expected_dictionary[key] = value
                key = line.split("\n")[0]
                value = ""
            else:
                if line.startswith('['):
                    value = ast.literal_eval(line)
                else:
                    value = value + line.lstrip()

        expected_dictionary[key] = value 
        dictionary = get_key_value_map(test_dir + "test_gp_part_5m_input.txt")
        expected_list = []
        current_list = []
        for key in sorted(expected_dictionary.keys()):
            expected_list.append(key)
            expected_list.append(expected_dictionary[key])
        for key in sorted(dictionary.keys()):
            current_list.append(key)
            current_list.append(dictionary[key])
        self.assertEqual(expected_list, current_list)

def main():
    """Calls unittest test."""
    unittest.main()

main()
