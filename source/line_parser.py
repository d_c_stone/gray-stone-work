#! /usr/bin/env python3.5
"""Parses line and creates dictionary."""
#! /usr/bin/env python3.5


from file_reader import get_line_list

def get_key_value_map(filename):
    """This makes dictionary."""
    key_values = {}
    header = ''
    comment = ''
    data = []

    lines = get_line_list(filename)

    for line in lines:
        if line.startswith(":"):
            header = header + line
        elif line.startswith("#"):
            comment = comment + line
        else:
            data.append(line.strip())

    key_values['header'] = header
    key_values['comment'] = comment
    key_values['data'] = data
    return key_values



