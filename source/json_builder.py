#! /usr/bin/env python3.5
"""Makes a json and prints it out to a file.
"""
#! /usr/bin/env python3.5
import json

from line_parser import get_key_value_map
from data_parser import get_key_value


def make_json():
    """Makes a json file from dictionary."""
    filename = "../tests/test_gp_part_5m_input.txt"
    output_file = "output.txt"
    key_values = get_key_value_map(filename)
    json_element = {}
    data_list = key_values.get("data")
    data = data_list[0]
    my_list = []
    json_element['header'] = key_values.get("header")
    json_element['comment'] = key_values.get("comment")
    for data in data_list:
        my_list.append(get_key_value(data))
    json_element['data'] = my_list
    file_object = open(output_file, "w")
    file_object.write(json.dumps(json_element, indent=4, separators=(',', ':')))
    file_object.close()

make_json()
