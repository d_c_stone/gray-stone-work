#! /usr/bin/env python3.5
"""This file creates a format."""
#! /usr/bin/env python3.5

def get_key_value(data):
    """Creates format array of key:value."""
    key_value = {}
    datetime_str = data[:16]
    data_str = data[19:]
    key_value[datetime_str] = data_str
    return key_value



