#! /usr/bin/env python3.5
"""This reads data from a file."""
#! /usr/bin/env python3.5

def get_line_list(filename):
    """This reads data from a file."""
    lines = []
    with open(filename, "r") as in_file:
        for line in in_file:
            lines.append(line)
    return lines

